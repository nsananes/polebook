import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
	Switch
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import {
  addAlert
} from '../../actions';
import {
  white,
  blue,
  grey,
  btnM
} from '../../global/variables';

const solene = require('../../img/solene.jpg');
const polebook = require('../../img/polebook.png');

// const {  width, height} = Dimensions.get('screen');

const Intro = React.createClass({
  render() {
    return (
    <View style={styles.container}>
      <View style={styles.innerBox}>
      	<View style={styles.profileBox}>
	      	<View style={styles.imgBox}>
	          <Image
	            source={solene}
	            style={{
	              flex: 1,
	              width: null,
	              height: null,
	              resizeMode: 'contain',
	              borderRadius: 55
	            }}
	          />
	      	</View>
          <Text style={styles.surname}>PETRY</Text>
          <Text style={styles.name}>SOLÈNE</Text>
          <Text style={styles.dep}>- INTERNE -</Text>
	      </View>
	      <View style={styles.btnBox}>
	        <View style={styles.innerBtnBox}>
	          <View style={styles.leftHalf}>
              <Button 
                onPress={() => Actions.tabbar()}
                containerStyle={styles.btn}
              >
                <Text style={styles.leftTxt}>+</Text>
              </Button>
	          </View>
	          <View style={styles.rightHalf}>
	            <Text style={styles.rightTxt}>DISPONIBLE</Text>
	          </View>
	        </View>
	        <View style={styles.innerBtnBox}>
	          <View style={styles.leftHalf}>
              <Switch
                onValueChange={
                  () => {
                    console.log('AAA');
                  }
                }
                style={styles.switch}
              />
	          </View>
	          <View style={styles.rightHalf}>
	            <Text style={styles.rightTxt}>GARDE</Text>
	          </View>
	        </View>
	        <View style={styles.innerBtnBox}>
	          <View style={styles.leftHalf}>
              <Switch
                onValueChange={
                  () => {
                    console.log('AAA');
                  }
                }
                style={styles.switch}
              />
	          </View>
	          <View style={styles.rightHalf}>
	            <Text style={styles.rightTxt}>OCCUPE</Text>
	          </View>
	        </View>
	        <View style={styles.innerBtnBox}>
	          <View style={styles.leftHalf}>
              <Switch
                onValueChange={
                  () => {
                    console.log('AAA');
                  }
                }
                style={styles.switch}
              />
	          </View>
	          <View style={styles.rightHalf}>
	            <Text style={styles.rightTxt}>FORMATION</Text>
	          </View>
	        </View>
	        <View style={styles.innerBtnBox}>
	          <View style={styles.leftHalf}>
              <Switch
                onValueChange={
                  () => {
                    console.log('AAA');
                  }
                }
                style={styles.switch}
              />
	          </View>
	          <View style={styles.rightHalf}>
	            <Text style={styles.rightTxt}>ABSENT</Text>
	          </View>
	        </View>
	      </View>
	      <View style={styles.logoBox}>
          <Image
            source={polebook}
            style={{
              flex: 1,
              width: null,
              height: null,
              resizeMode: 'contain'
            }}
          />
	      </View>
      </View>
    </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 25
  },
  innerBox: {
  	flex: 1,
  	backgroundColor: grey,
  	borderRadius: 20,
  	alignItems: 'stretch'
  },
  imgBox: {
  	alignSelf: 'center',
  	borderRadius: 55,
  	width: 110,
  	height: 110,
  	marginTop: 20
  },
  surname: {
  	fontSize: 22,
  	alignSelf: 'center',
  	fontWeight: '700',
  	color: blue,
  	marginTop: 15
  },
  name: {
  	fontSize: 17,
  	alignSelf: 'center',
  	fontWeight: '300',
  	color: blue,
  	marginTop: -6
  },
  dep: {
  	fontSize: 15,
  	alignSelf: 'center',
  	fontWeight: '300',
  	color: blue,
  	marginTop: -3
  },
  btnBox: {
  	flex: 1,
  	alignItems: 'stretch',
  	justifyContent: 'center',
  	paddingVertical: 20
  },
  innerBtnBox: {
  	flex: 1,
  	flexDirection: 'row',
  	alignItems: 'center',
  	justifyContent: 'center'
  },
  rightHalf: {
  	flex: 6
  },
  leftHalf: {
  	flex: 4,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  rightTxt: {
  	color: blue,
  	fontSize: 17,
  	fontWeight: '700'
  },
  logoBox: {
  	height: 40,
  	paddingVertical: 10,
  	alignItems: 'stretch',
  	justifyContent: 'center'
  },
  btn: btnM,
  leftTxt: {
  	color: white,
  	fontWeight: '700',
  	fontSize: 25
  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(Intro);
