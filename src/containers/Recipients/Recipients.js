import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import ListConversations from '../../components/ListConversations/ListConversations';
import {
  addAlert
} from '../../actions';
import {
  backgroundColor2
} from '../../global/variables';

const listData = [
  {
    id: '000001',
    group: 'Gyneco',
    txt: 'Je viens de voir sur l’appli que...',
    userId: '999',
    firstName: 'Massimo',
    familyName: 'Pibiri',
    date: '34567890'
  },
  {
    id: '000002',
    group: 'Primaires',
    txt: 'Nico... urgence en gyneco. Peux tu...',
    userId: '888',
    firstName: 'Benoit',
    familyName: 'Trinité',
    date: '34567891'
  },
  {
    id: '000003',
    group: 'Ortopedie',
    txt: 'On va se faire une biere por fêter ca?',
    userId: '999',
    firstName: 'Massimo',
    familyName: 'Pibiri',
    date: '34567892'
  },
  {
    id: '000004',
    group: 'Urgences',
    txt: 'Encore fatigué depuis hier...',
    userId: '888',
    firstName: 'Benoit',
    familyName: 'Trinité',
    date: '34567893'
  },
  {
    id: '000005',
    group: 'François',
    txt: ':-(',
    userId: '999',
    firstName: 'Massimo',
    familyName: 'Pibiri',
    date: '34567894'
  }
];

const Recipients = React.createClass({
  goToConversation(id) {
    Actions.chat({id});
  },
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.listBox}>
          <ListConversations
            messages={listData}
            goToConversation={this.goToConversation}
          />
        </View>
        <ContainerAlerts/>
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: backgroundColor2
  },
  listBox: {
    flex: 1
  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(Recipients);
