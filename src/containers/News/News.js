import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Image
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import ListNews from '../../components/ListNews/ListNews';
import {
  addAlert,
  writeMessage
} from '../../actions';
import {
  backgroundColor2,
  white,
  btnMcenter,
  green,
  blue
} from '../../global/variables';

const solene = require('../../img/solene.jpg');

const listData = [
  {
    id: '000001',
    txt: 'Nicolas a partagé Congrès MAJ : Evidence-based labor delivery management PDF',
    userId: '999',
    firstName: 'Alexander',
    familyName: 'Fleming',
    date: '34567890'
  },
  {
    id: '000002',
    txt: 'Nicolas a publié TIO & Breast Cancer PDF',
    userId: '999',
    firstName: 'Jean-Martin',
    familyName: 'Charcot',
    date: '34567890'
  },
  {
    id: '000003',
    txt: 'Victor a publié « Attention, aujourd’hui, grève du self ! »',
    userId: '999',
    firstName: 'Massimo',
    familyName: 'Pibiri',
    date: '34567890'
  }
];

const News = React.createClass({
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.profile}>
          <Image
            source={solene}
            style={{
              flex: 1,
              width: null,
              height: null,
              resizeMode: 'contain'
            }}
          />
          <View style={styles.active}/>
        </View>
        <View style={styles.header}>
          <View style={styles.left}>
            <Button 
              onPress={() => Actions.tabbar()}
              containerStyle={styles.btn}
            >
              <Text style={styles.leftTxt}>+</Text>
            </Button>
          </View>
        </View>
        <View style={styles.listBox}>
          <ListNews
            messages={listData}
          />
        </View>
        <ContainerAlerts/>
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    position: 'relative'
  },
  input: {
    borderRadius: 3,
    height: 43,
    backgroundColor: white,
    marginVertical: 30,
    marginHorizontal: 20,
    fontSize: 19
  },
  listBox: {
    flex: 1
  },
  header: {
    paddingTop: 10,
    paddingBottom: 15,
    borderBottomColor: blue,
    borderBottomWidth: 2,
    marginHorizontal: 40,
    marginTop: 40
  },
  btn: btnMcenter,
  leftTxt: {
    color: white,
    fontWeight: '700',
    fontSize: 25
  },
  profile: {
    position: 'absolute',
    top: 10,
    right: 20,
    width: 80,
    height: 80,
    borderRadius: 30
  },
  active: {
    width: 32,
    height: 32,
    borderRadius: 16,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: green
  }
});

function mapStateToProps(state) {
  return {
    newText: state.form.txt
  };
}

export default connect(mapStateToProps)(News);
