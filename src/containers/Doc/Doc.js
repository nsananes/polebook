import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import {
  addAlert
} from '../../actions';
import {
  white,
  blue,
  grey,
  btnM
} from '../../global/variables';

const actu = require('../../img/fil-actu-2.png');
const left = require('../../icons/left.png');

// const {  width, height} = Dimensions.get('screen');

const Doc = React.createClass({
  render() {
    return (
    <View style={styles.container}>
      <Button 
        onPress={() => Actions.newsBase()}
        containerStyle={styles.btn}
      >
        <Image
          source={left}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </Button>
      <Image
        source={actu}
        style={{
          flex: 1,
          width: null,
          height: null,
          resizeMode: 'contain'
        }}
      >
      </Image>
    </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    marginBottom: -20
  },
  btn: {
    position: 'absolute',
    top: 10,
    left: 5,
    width: 30,
    height: 30
  },
  leftTxt: {

  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(Doc);
