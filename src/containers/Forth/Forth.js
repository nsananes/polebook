import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import {
  addAlert
} from '../../actions';
import {
  white,
  blue,
  grey,
  btnM
} from '../../global/variables';

const calendar = require('../../img/calendrier4.png');

// const {  width, height} = Dimensions.get('screen');

const Forth = React.createClass({
  render() {
    return (
    <View style={styles.container}>
      <Image
        source={calendar}
        style={{
          flex: 1,
          width: null,
          height: null,
          resizeMode: 'cover'
        }}
      >
      </Image>
    </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,marginHorizontal: -10,
    marginBottom: -80
  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(Forth);
