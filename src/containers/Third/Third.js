import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import {
  addAlert
} from '../../actions';
import {
  white,
  blue,
  grey,
  btnM
} from '../../global/variables';

const push = require('../../img/push1.png');

// const {  width, height} = Dimensions.get('screen');

const Third = React.createClass({
  render() {
    return (
    <View style={styles.container}>
      <Image
        source={push}
        style={{
          flex: 1,
          width: null,
          height: null,
          resizeMode: 'cover'
        }}
      >
      </Image>
    </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    marginBottom: -70,
    marginHorizontal: -10
  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(Third);
