import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import ListMessages from '../../components/ListMessages/ListMessages';
import {
  addAlert,
  writeMessage
} from '../../actions';
import {
  white,
  black,
  green,
  grey
} from '../../global/variables';

const solene = require('../../img/solene.jpg');

const listData = [
  {
    id: '000002',
    txt: 'Je viens de voir sur l’appli que tu étais  l’interne de garde et la seule dispo. Il y a 3 heures d’attente aux urgences, tu peux venir en renfort ?!!',
    userId: '999',
    firstName: 'Nicole',
    familyName: 'Clement',
    date: '19 MAR, 9:40'
  },
  {
    id: '000001',
    txt: 'Ok, j’arrive !',
    userId: '888',
    firstName: 'Massimo',
    familyName: 'Pibiri',
    date: '19 MAR, 9:45'
  }
];

const Chat = React.createClass({
  writeNewText() {
  },
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.profile}>
          <Image
            source={solene}
            style={{
              flex: 1,
              width: null,
              height: null,
              resizeMode: 'contain'
            }}
          />
          <View style={styles.active}/>
        </View>
        <View style={styles.listBox}>
          <ListMessages messages={listData} />
        </View>
        <TextInput
          onChange={(event) => {
            if (event.nativeEvent.text && event.nativeEvent.text !== '') {
              this.props.dispatch(writeMessage(event.nativeEvent.text));
            }
          }}
          value={this.props.txt > 1 ? this.props.txt : null}
          returnKeyType='done'
          placeholder='Ecrire un message'
          placeholderTextColor="#555"
          underlineColorAndroid='rgba(0,0,0,0)'
          style={styles.input}
          onSubmitEditing={(event) => this.writeNewText(event.nativeEvent.text)}
        />
        <ContainerAlerts/>
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: grey,
    paddingBottom: 25,
    paddingTop: 90
  },
  profile: {
    position: 'absolute',
    top: 10,
    right: 20,
    width: 80,
    height: 80,
    backgroundColor: grey,
    borderRadius: 40
  },
  active: {
    width: 32,
    height: 32,
    borderRadius: 16,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: green
  },
  listBox: {
    backgroundColor: white
  },
  input: {
    borderRadius: 3,
    height: 43,
    backgroundColor: white,
    marginTop: 30,
    marginHorizontal: 20,
    fontSize: 19
  },
  listBox: {
    flex: 1,
    paddingTop: 30
  }
});

function mapStateToProps(state) {
  return {
    newText: state.form.txt
  };
}

export default connect(mapStateToProps)(Chat);
