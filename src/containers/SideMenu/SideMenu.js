import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ContainerAlerts from '../../components/Alerts/ContainerAlerts';
import {
  addAlert
} from '../../actions';

const SideMenu = React.createClass({
  render() {
    return (
    <View style={styles.container}>
      <Text>SideMenu</Text>
      <ContainerAlerts />
    </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 53,
    paddingBottom: 53,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red'
  }
});

function mapStateToProps() {
  return {
  };
}

export default connect(mapStateToProps)(SideMenu);
