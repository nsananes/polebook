import React from 'react';
import { Provider } from 'react-redux';
import {
  StyleSheet,
  AppRegistry,
  Text,
  TouchableOpacity,
  Image,
  View
} from 'react-native';
// tutorial to implement the router (https://www.youtube.com/watch?v=j8zfTSRadNg#t=306.006005) -> official documentation is really poor
import { Actions, Scene, Router } from 'react-native-router-flux';
import Intro from './containers/Intro/Intro';
import News from './containers/News/News';
import Doc from './containers/Doc/Doc';
import Recipients from './containers/Recipients/Recipients';
import Chat from './containers/Chat/Chat';
import Third from './containers/Third/Third';
import Forth from './containers/Forth/Forth';
import SideMenu from './containers/SideMenu/SideMenu';

// get the store created and connect it to the state
import { configureStore } from './store';
import {
  white,
  backgroundColor1
} from './global/variables';
import { API_URL } from './api';

const back = require('./icons/back.png');
const homeSans = require('./icons/home-sans.png');
const homeActive = require('./icons/home-actif.png');
const messagerieSans = require('./icons/messagerie-sans.png');
const messagerieActif = require('./icons/messagerie-actif.png');
const pushSans = require('./icons/push-sans.png');
const pushActif = require('./icons/push-actif.png');
const canlendrierSans = require('./icons/canlendrier-sans.png');
const calendrierActif = require('./icons/calendrier-actif.png');

window.navigator.userAgent = 'ReactNative';
const io = require('socket.io-client/dist/socket.io');
const socket = io('http://ec2-34-251-149-215.eu-west-1.compute.amazonaws.com:80', {
  transports: ['websocket'] // you need to explicitly tell it to use websockets
});

const TabIconNews = ({ selected, title }) => {
  if (selected) {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={homeActive}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  } else {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={homeSans}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  }
};

const TabIconMess = ({ selected, title }) => {
  if (selected) {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={messagerieActif}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  } else {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={messagerieSans}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  }
};

const TabIconPush = ({ selected, title }) => {
  if (selected) {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={pushActif}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  } else {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={pushSans}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  }
};

const TabIconCalendar = ({ selected, title }) => {
  if (selected) {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={calendrierActif}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  } else {
    return (
      <View style={{
        width: 30,
        height: 30,
        marginVertical: 7
      }}>
        <Image
          source={canlendrierSans}
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: 'contain'
          }}
        />
      </View>
    );
  }
};

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? 'orange' : 'grey' }}>{title}</Text>
  );
};


export default function native() {
  const Polebook = React.createClass({
    componentDidMount() {
      socket.emit('userJoined', '1');
      socket.on('message', (res) => {
        console.log(res);
      });
    },
    getRightButton() {
      return (
        <TouchableOpacity
          onPress={() => Actions.sidemenu()}
        >
          <Image
            style={{ width: 22, height: 22, marginRight: 15 }}
            source={require('./icons/menubuttonBlack.png')}
          />
        </TouchableOpacity>
      );
    },
    getRemoveButton() {
      return (
        <TouchableOpacity
          onPress={() => Actions.home()}
        >
          <Image
            style={{ width: 22, height: 22, marginRight: 15 }}
            source={require('./icons/cross.png')}
          />
        </TouchableOpacity>
      );
    },
    getBackButton() {
      return (
        <TouchableOpacity
          onPress={() => Actions.pop()}
        >
          <Image
            style={{ width: 22, height: 20, marginLeft: 10 }}
            source={back}
          />
        </TouchableOpacity>
      );
    },
    render() {
      return (
        <Provider store={configureStore()} >
          <Router /* renderLeftButton={this.navBarButton} */>
            <Scene key='root'>
                <Scene 
                  key='intro'
                  component={Intro}
                  hideNavBar 
                />
                {/* TABS MENU */}
                <Scene 
                  key='tabbar'
                  tabs
                  hideNavBar 
                  initial
                  tabBarStyle={{ backgroundColor: 'white' }}
                  type='replace'
                >
                  {/* NEWS PAGE */}
                  <Scene key='newsBase' title='News' icon={TabIconNews}>
                    <Scene 
                      key='news'
                      component={News}
                      title='NewsPage'
                      hideNavBar 
                      renderRightButton={this.getRightButton}
                    />
                    <Scene 
                      key='doc'
                      component={Doc}
                      barButtonTextStyle={styles.barButtonTextStyle}
                      barButtonIconStyle={styles.barButtonIconStyle}
                      renderBackButton={this.getBackButton}
                    />
                  </Scene>
                  {/* FREEZES PAGE */}
                  <Scene key='third' title='Third' icon={TabIconPush}>
                    <Scene 
                      key='thirdpage'
                      component={Third}
                      title='IntroFreeze'
                      hideNavBar 
                      renderRightButton={this.getRightButton}
                    />
                  </Scene>
                  {/* HOME PAGE */}
                  <Scene key='recipientsBase' title='Recipients' icon={TabIconMess}>
                    <Scene 
                      key='recipients'
                      component={Recipients}
                      title='RecipientsPage'
                      initial
                      hideNavBar 
                      renderRightButton={this.getRightButton}
                    />
                    <Scene 
                      key='chat'
                      component={Chat}
                      title='Chat'
                      navigationBarStyle={styles.navBar}
                      titleStyle={styles.navTitle}
                      sceneStyle={styles.routerScene}
                      hideTabBar
                      barButtonTextStyle={styles.barButtonTextStyle}
                      barButtonIconStyle={styles.barButtonIconStyle}
                      renderBackButton={this.getBackButton}
                    />
                  </Scene>
                  {/* NOTIFICATIONS PAGE */}
                  <Scene key='forth' title='Forth' icon={TabIconCalendar}>
                    <Scene 
                      key='forthpage'
                      component={Forth}
                      hideNavBar 
                      title='Notifications'
                      renderRightButton={this.getRightButton}
                    />
                  </Scene>
                </Scene>
                <Scene 
                  key='sidemenu'
                  component={SideMenu}
                  title='SideMenu'
                  renderRightButton={this.getRightButton}
                />
              </Scene>
          </Router>
        </Provider>
      );
    }
  });

  const styles = StyleSheet.create({
    navBar: {
      height: 50,
      backgroundColor: backgroundColor1,
      marginTop: 0,
      borderBottomWidth: 0
    },
    tabBar: {
      height: 40,
      backgroundColor: backgroundColor1,
      marginTop: 0,
      borderBottomWidth: 0
    },
    navTitle: {
      color: white, // changing navbar title color
    },
    routerScene: {
      paddingTop: 0, // some navbar padding to avoid content overlap
    },
    barButtonTextStyle: {
      color: '#F16B6F'
    },
    barButtonIconStyle: {
        tintColor: '#F16B6F'
    }
  });

  AppRegistry.registerComponent('polebook', () => Polebook);
}
