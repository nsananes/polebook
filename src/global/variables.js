export const backgroundColor1 = 'pink';
export const backgroundColor2 = 'grey';
export const backgroundActive = 'grey';

export const borderMedium = 'grey';

export const white = '#fff';
export const black = '#555';
export const blue = '#6899FF';
export const grey = '#F5F5F5';
export const green = '#44FFD3';
export const yellow = '#FFE706';

export const btnM = {
	backgroundColor: '#44FFD3',
	width: 60,
	borderRadius: 25,
	justifyContent: 'center',
	alignItems: 'flex-end',
	height: 50,
	width: 90,
	padding: 15
};

export const btnMcenter = {
	backgroundColor: '#44FFD3',
	width: 60,
	borderRadius: 25,
	justifyContent: 'center',
	alignItems: 'center',
	height: 50,
	width: 90,
	padding: 15
};
