import React from 'react';
import {
  StyleSheet,
  ListView,
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
  white,
  borderMedium,
  yellow,
  blue
} from '../../global/variables';

const defaultImg = require('../../icons/user.png');
const victor = require('../../img/victor.png');
const nicolas = require('../../img/nicolas.png');
const attach = require('../../icons/attach.png');
const apercu = require('../../icons/apercu.png');
const download = require('../../icons/download.png');

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});

const ListNews = React.createClass({
  render() {
    const {
      messages
    } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View
            key={messages[0].id}
            style={styles.itemBox}
            onPress={
              () => this.props.goToConversation(messages[0].id)
            }
          >
            <View style={styles.item}>
              <View style={styles.header}>
                <View style={styles.imgBox}>
                  <Image
                    source={nicolas}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </View>
              </View>
              <View style={styles.txtBox}>
                <Text style={styles.txt}>{messages[0].txt}</Text>
                <TouchableOpacity
                  style={styles.iconBox}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={apercu}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.iconBox2}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={download}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {/* -------------------- */}
          <View
            key={messages[1].id}
            style={styles.itemBox}
            onPress={
              () => this.props.goToConversation(messages[1].id)
            }
          >
            <View style={styles.item}>
              <View style={styles.header}>
                <View style={styles.imgBox}>
                  <Image
                    source={nicolas}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </View>
              </View>
              <View style={styles.txtBox}>
                <Text style={styles.txt}>{messages[1].txt}</Text>
                <TouchableOpacity
                  style={styles.iconBox}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={apercu}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.iconBox2}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={download}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {/* -------------------- */}
          <View
            key={messages[2].id}
            style={styles.itemBox}
            onPress={
              () => this.props.goToConversation(messages[2].id)
            }
          >
            <View style={styles.item}>
              <View style={styles.header}>
                <View style={styles.imgBox}>
                  <Image
                    source={victor}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </View>
              </View>
              <View style={styles.txtBox}>
                <Text style={styles.txt}>{messages[2].txt}</Text>
                <TouchableOpacity
                  style={styles.iconBox}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={apercu}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.iconBox2}
                  onPress={() => Actions.doc()}
                >
                  <Image
                    source={download}
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  header: {
    height: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  headerTxt: {
    flex: 1,
    alignItems: 'flex-start'
  },
  txt: {
    fontWeight: '700',
    color: blue,
    fontSize: 19,
    color: blue
  },
  itemBox: {
    flex: 1
  },
  item: {
    backgroundColor: yellow,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    marginHorizontal: 40,
    marginVertical: 18,
    paddingHorizontal: 40,
    paddingTop: 30,
    paddingBottom: 45,
    position: 'relative',
    overflow: 'visible'
  },
  imgBox: {
    width: 50,
    height: 50,
    borderRadius: 23,
    position: 'absolute',
    top: -27,
    left: -35
  },
  txtBox: {
    flex: 1,
    paddingTop: 15
  },
  groupTxt: {
    fontSize: 19
  },
  iconBox: {
    width: 45,
    height: 40,
    position: 'absolute',
    bottom: -40,
    right: 10
  },
  iconBox2: {
    width: 45,
    height: 40,
    position: 'absolute',
    bottom: -40,
    right: -35
  }
});

export default ListNews;
