import React from 'react';
import {
  StyleSheet,
  ListView,
  View,
  Image,
  Text,
  TouchableHighlight
} from 'react-native';
import {
  white,
  borderMedium
} from '../../global/variables';

const defaultImg = require('../../icons/user.png');

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});

const ListMessages = React.createClass({
  _renderRow(rowData) {
    return (
      <TouchableHighlight
        key={rowData.id}
        style={styles.itemBox}
        onPress={
          () => this.props.goToConversation(rowData.id)
        }
      >
        <View style={styles.item}>
          <View style={styles.imgBox}>
            <Image
              source={defaultImg}
              style={{
                flex: 1,
                width: null,
                height: null,
                resizeMode: 'contain'
              }}
            />
          </View>
          <View style={styles.txtBox}>
            <Text style={styles.groupTxt}>{rowData.group}</Text>
            <Text style={styles.txt}>{rowData.txt}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  },
  render() {
    return (
      <View style={styles.container}>
        { this.props.messages ?
          <ListView 
            dataSource={ds.cloneWithRows(this.props.messages)}
            renderRow={this._renderRow}
          />
          :
          <Text>Pas encore de messages</Text>
        }
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  itemBox: {
    flex: 1
  },
  item: {
    backgroundColor: white,
    borderBottomColor: borderMedium,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 22
  },
  imgBox: {
    width: 46,
    height: 46,
    borderColor: borderMedium,
    borderWidth: 1,
    padding: 5,
    borderRadius: 23
  },
  txtBox: {
    flex: 1,
    paddingHorizontal: 10
  },
  groupTxt: {
    fontSize: 19
  }
});

export default ListMessages;
