import React from 'react';
import {
  StyleSheet,
  ListView,
  View,
  Image,
  Text,
  Dimensions
} from 'react-native';
import {
  white,
  borderMedium,
  grey,
  blue
} from '../../global/variables';

const defaultImg = require('../../icons/user.png');

const { width } = Dimensions.get('screen');

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});

const me = '000001';

const ListMessages = React.createClass({
  _renderRow(rowData) {
    if (me === rowData.id) {
      return (
        <View
          key={rowData.id}
          style={styles.itemMe}
        >
          <Text style={styles.txt}>{rowData.txt}</Text>
        </View>
      );
    } else {
      return (
        <View
          key={rowData.id}
          style={styles.item}
        >
          <Text style={styles.writer}>{rowData.firstName} {rowData.familyName}</Text>
          <Text style={styles.txt}>{rowData.txt}</Text>
        </View>
      );
    }
  },
  render() {
    return (
      <View style={styles.container}>
        { this.props.messages ?
          <ListView 
            dataSource={ds.cloneWithRows(this.props.messages)}
            renderRow={this._renderRow}
          />
          :
          <Text>Pas encore de messages</Text>
        }
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: grey,
    paddingHorizontal: 20
  },
  item: {
  	backgroundColor: white,
  	justifyContent: 'center',
  	alignItems: 'flex-start',
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: (width / 6) * 5,
    borderRadius: 5,
    marginVertical: 8,
    marginTop: 40
  },
  itemMe: {
    backgroundColor: white,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: (width / 6) * 5,
    borderRadius: 5,
    marginVertical: 8
  },
  writer: {
    fontSize: 17,
    fontWeight: '700'
  },
  txt: {
    fontSize: 17,
    fontWeight: '300',
    color: blue,
    fontSize: 19,
    fontWeight: '700'
  }
});

export default ListMessages;
