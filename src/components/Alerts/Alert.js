import React from 'react';
import { connect } from 'react-redux';
import {
  Image,
	StyleSheet,
	View,
	Text,
	TouchableWithoutFeedback
} from 'react-native';

import { removeAlert } from '../../actions';

const removeIc = require('./icones/remove.png');

const Alert = React.createClass({
  onRemoveAlert() {
    this.props.dispatch(removeAlert(this.props.alert.id));
  },
  render() {
    return (
      <TouchableWithoutFeedback onPress={this.onRemoveAlert}>
        { this.props.alert.kind === 'danger' ?
          <View style={styles.containerDanger}>
            <View style={styles.removeBox}>
              <Image
                source={removeIc}
                style={styles.removeIc}
                resizeMode='cover'
              />
            </View>
            <Text style={styles.textDanger}>
              {this.props.alert.text}
            </Text>
          </View>
          :
          <View style={styles.container}>
            <View style={styles.removeBox}>
              <Image
                source={removeIc}
                style={styles.removeIc}
                resizeMode='cover'
              />
            </View>
            <Text style={styles.text}>
              {this.props.alert.text}
            </Text>
          </View>
        }
      </TouchableWithoutFeedback>
    );
  }
});

const styles = StyleSheet.create({
  containerDanger: {
    flex: 1,
    flexDirection: 'row',
    padding: 16,
    backgroundColor: 'rgba(154,43,17,0.85)',
    borderColor: '#ebccd1',
    borderTopWidth: 1,
    position: 'relative',
    zIndex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    padding: 16,
    backgroundColor: 'rgba(55,55,55,0.85)',
    borderColor: '#ebccd1',
    borderTopWidth: 1,
    position: 'relative',
    zIndex: 1,
  },
  removeBox: {
    position: 'absolute',
    top: 4,
    right: 5,
    padding: 0,
    zIndex: 10,
  },
  removeIc: {
    height: 20,
    width: 20
  },
  text: {
    color: 'white'
  },
  textDanger: {
    color: 'white'
  }
});

export default connect()(Alert);
