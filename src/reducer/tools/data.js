const defaultState = {
	txt: undefined
};

module.exports = (state = defaultState, action) => {
	switch (action.type) {
    case 'WRITE_MESSAGE':
      return {
        ...state,
        txt: action.txt
      };
    default:
      return state;
	}
};
