// combine reducers to get a unique global reducer
'use strict';

import { combineReducers } from 'redux';
import alerts from './alerts/message';
import form from './tools/data';

const rootReducer = combineReducers({
  alerts,
  form
});

export default rootReducer;
